﻿using System;

namespace DoNotLookInto
{
    public class ExternalSystem
    {
        int seed = 543121;
        public string GetDateTimeFormat()
        {
            return "yyyymmdd";
        }

        private string GetRightTimeFormat()
        {
            return "yyyyMMdd";
        }
        public int SubmitData(string dateTimeString)
        {
            Random r = new Random(seed); 
            var returnValue = Math.Abs(r.Next());
            seed = returnValue;
            return returnValue;
        }
    }
}
