﻿using ProcessorLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Question
{
    static class DebugLogger
    {
        public static string Storage { get; set; }
        public static List<int> ListCopy { get; set; } = new List<int>();
        public static List<int> EmptyList { get; set; } = new List<int>();
    }

    public class TempList : IList<int>
    {
        public int this[int index] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(int item)
        {
            DebugLogger.ListCopy.Add(item);
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(int item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(int[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<int> GetEnumerator()
        {
            return DebugLogger.EmptyList.GetEnumerator();
        }

        public int IndexOf(int item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, int item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return DebugLogger.EmptyList.GetEnumerator();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<DateTime> datesToProcess = new List<DateTime>
            {
                new DateTime(2019,2,2),
                new DateTime(2019,2,6),
                new DateTime(2019,3,1),
                new DateTime(2019,1,1),
                new DateTime(2019,8,21),
                new DateTime(2019,12,23),
                new DateTime(2019,2,24),
                new DateTime(2019,2,25),
            };
            var processor = new Processor();
            processor.GetType().GetProperty("DateTimeFormat", BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(processor, "yyyyMMdd", null);
            processor.GetType().GetProperty("AggregatedStoredBytes", BindingFlags.NonPublic | BindingFlags.Instance)
               .SetValue(processor, new TempList(), null);

            var datesToProcessEnumerable = datesToProcess.Select(p =>
            {
                DebugLogger.Storage = p.ToString();
                Console.WriteLine(DebugLogger.Storage);
                PropertyInfo prop =
                typeof(Processor).GetProperty("DateTimeFormat", BindingFlags.NonPublic | BindingFlags.Instance);

                MethodInfo getter = prop.GetGetMethod(nonPublic: true);
                string dateTimeFormatFound = (string) getter.Invoke(processor, null);
                Console.WriteLine("Attention! DateTimeFormat!" + dateTimeFormatFound);
                return p;
            });

            var result = processor.Process(datesToProcessEnumerable);

            List<long> longs = DebugLogger.ListCopy.ConvertAll(i => (long)i);
            Console.WriteLine("Result is " + longs.Sum());
        }
    }
}
