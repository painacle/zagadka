﻿using DoNotLookInto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessorLibrary
{
    public class Processor
    {
        string DateTimeFormat { get; set; }
        IList<int> AggregatedStoredBytes { get; set; } 
        public Processor()
        {
            AggregatedStoredBytes = new List<int>();
        }
        public int Process(IEnumerable<DateTime> datesToProcess)
        {
            try
            {
                var externalSystem = new ExternalSystem();
                if (DateTimeFormat == null)
                    DateTimeFormat = externalSystem.GetDateTimeFormat();
                foreach (var dateToProcess in datesToProcess)
                    AggregatedStoredBytes.Add(externalSystem.SubmitData(dateToProcess.ToString(DateTimeFormat)));
                return AggregatedStoredBytes.Sum(bytes => bytes);
            }
            catch (Exception)
            {
                Environment.Exit(1);
                return -1;
            }
        }
    }
}
