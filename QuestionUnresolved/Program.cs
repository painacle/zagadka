﻿using ProcessorLibrary;
using System;
using System.Collections.Generic;

namespace QuestionUnresolved
{
    class Program
    {
        static void Main(string[] args)
        {
            var datesToProcess = new List<DateTime>
            {
                new DateTime(2019,2,2),
                new DateTime(2019,2,6),
                new DateTime(2019,3,1),
                new DateTime(2019,1,1),
                new DateTime(2019,8,21),
                new DateTime(2019,12,23),
                new DateTime(2019,2,24),
                new DateTime(2019,2,25),
            };
            var processor = new Processor();
            var result = processor.Process(datesToProcess);
            Console.WriteLine(result);
        }
    }
}
